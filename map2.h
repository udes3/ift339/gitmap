//
//  map2.h
//  MAP-AVL
//
//  Jean Goulet 17-03-17.
//  Copyleft 2017 UdeS
//

#ifndef map2_h
#define map2_h


///////////////////////////////////////////////////////////////////////////
// lower_bound, upper_bound et find

template <typename Tclef, typename Tvaleur>
typename map<Tclef,Tvaleur>::iterator map<Tclef,Tvaleur>::lower_bound(const Tclef& c)const{
    noeud* LowerB = APRES;
    noeud* p;
    for (p = RACINE; p != nullptr;) {
        if (plus_petit(p, c)){
            p = p->DROITE;
        } else {
            LowerB = p;
            p = p->GAUCHE;
        }
        
    }
    return iterator(LowerB);
}

template <typename Tclef, typename Tvaleur>
typename map<Tclef,Tvaleur>::iterator map<Tclef,Tvaleur>::upper_bound(const Tclef& c)const{
    iterator pos=lower_bound(c);
    if (pos != end() && !plus_petit(pos.POINTEUR, c) && !plus_petit(c, pos.POINTEUR)) {
        return ++pos;
    }
    return pos;
}

template <typename Tclef, typename Tvaleur>
typename map<Tclef, Tvaleur>::iterator map<Tclef, Tvaleur>::find(const Tclef& c)const{
    iterator pos(lower_bound(c));
    if (pos.POINTEUR->CONTENU == nullptr || !plus_petit(pos.POINTEUR, c) && !plus_petit(c, pos.POINTEUR))
        return pos;
    else
        return pos = iterator(APRES);
}

///////////////////////////////////////////////////////////////////////////
//insert avec indice

template <typename Tclef, typename Tvaleur>
typename map<Tclef,Tvaleur>::iterator map<Tclef,Tvaleur>::insert(iterator j,const Tclef& c){
    noeud* p=j.POINTEUR;
    if (plus_petit(p, c) && plus_petit(c,(++j).POINTEUR)) {
        if (p->DROITE == nullptr) {
            p->DROITE = new noeud(c, p);
            --(p->INDICE);
        }
        else {
            p->DROITE->GAUCHE = new noeud(c, p->DROITE);
            --(p->INDICE);
            ++(p->DROITE->INDICE);
        }
    }
    else if (plus_petit(c, p) && plus_petit((--j).POINTEUR, c)) {
        if (p->GAUCHE == nullptr) {
            p->GAUCHE = new noeud(c, p);
            ++(p->INDICE);
        }
        else {
            p->GAUCHE->DROITE = new noeud(c, p->GAUCHE);
            ++(p->INDICE);
            --(p->GAUCHE->INDICE);
        }
    }
    else{ 
        insert(c,RACINE,j);
        return iterator(APRES);
    }
    ++SIZE;
    for (p = p->PARENT; p->INDICE!=0; p = p->PARENT) {
        if (plus_petit(p->CONTENU->first, p->PARENT))
            allonger_a_gauche(creer_reference(p));
        else
            allonger_a_droite(creer_reference(p));
    }
    return j;
}


///////////////////////////////////////////////////////////////////////////
//erase a partir d'une position

template <typename Tclef, typename Tvaleur>
typename map<Tclef,Tvaleur>::iterator map<Tclef,Tvaleur>::erase(iterator i){
    // STUB prend un temps O(log n) et non O(1) amorti
    if(i==end())throw(std::out_of_range("À la fin"));
    erase(i++->first);
    return i;
}



///////////////////////////////////////////////////////////////////////////
// gestion de l'equilibre de l'arbre


//effectuer une rotation simple de la gauche vers la droite
template <typename Tclef, typename Tvaleur>
void map<Tclef,Tvaleur>::rotation_gauche_droite(noeud*& p){
    noeud* temp = p->GAUCHE;
    int ia = temp->INDICE;
    int ib = p->INDICE;
    int nib = -ia - max(0, -ia) - 1 + ib;
    int nia = ia - max(0, -nib) - 1;
    temp->INDICE = nia;
    p->INDICE = nib;
    p->GAUCHE = temp->DROITE;
    if (temp->DROITE != nullptr) {
        p->GAUCHE->PARENT = p;
    }
    temp->DROITE = p;
    temp->PARENT = p->PARENT;
    p->PARENT = temp;
    p = temp;
}

//effectuer une rotation simple de la droite vers la gauche
template <typename Tclef, typename Tvaleur>
void map<Tclef,Tvaleur>::rotation_droite_gauche(noeud*& p){
    noeud* temp = p->DROITE;
    int ia = p->INDICE;
    int ib = temp->INDICE;
    int nia = ia + max(0, -ib) + 1;
    int nib = ib + nia +  max(0, -nia) + 1;
    p->INDICE = nia;
    temp->INDICE = nib;
    p->DROITE = temp->GAUCHE;
    if (temp->GAUCHE != nullptr) {
        p->DROITE->PARENT = p;
    }
    temp->GAUCHE = p;
    temp->PARENT = p->PARENT;
    p->PARENT = temp;
    p = temp;
}


#endif /* map2_h */
